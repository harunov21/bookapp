package com.example.bookapp.Model;

import com.example.bookapp.Enums.UserType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.HashCodeExclude;
import org.apache.commons.lang3.builder.ToStringExclude;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Customer {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
         Long id;
         String name;
         String surname;
         String email;
         String password;
         boolean isActive;

         UserType userType;

        @ManyToMany(mappedBy = "customers")
         List<Book> books = new ArrayList<>();



}

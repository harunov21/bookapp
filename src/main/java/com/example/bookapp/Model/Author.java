package com.example.bookapp.Model;

import com.example.bookapp.Enums.UserType;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    String surname;
    String email;
    String password;
    Boolean isActive;
    UserType userType;

    @OneToMany(mappedBy ="author" , cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    List<Book> books;

    }


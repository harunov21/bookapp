package com.example.bookapp.Dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BooksRequestDto {

    Long id;
    String title;
    Long authorId;
    Long customerId;

}

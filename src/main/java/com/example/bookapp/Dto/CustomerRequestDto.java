package com.example.bookapp.Dto;

import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;


@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRequestDto {


    Long id;
    String CustomerName;
    private List<BooksRequestDto> books = new ArrayList<>();

}

package com.example.bookapp.Dto;

import jakarta.persistence.Column;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.UniqueElements;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerLogInRequestDto {
    @Column(unique = true,nullable = false)
    String email;

    @Column(nullable = false)
    String password;
}

package com.example.bookapp.Dto;

import jakarta.persistence.Column;
import lombok.*;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthorLogInRequestDto {
    @Column(unique = true,nullable = false)
    String email;

    @Column(nullable = false)
    String password;

}

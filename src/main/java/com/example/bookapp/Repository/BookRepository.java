package com.example.bookapp.Repository;

import com.example.bookapp.Model.Author;
import com.example.bookapp.Model.Book;
import jakarta.persistence.Id;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book,Id> {
    Optional<Book> findById(Long bookId);

    @Query("SELECT b FROM Book b WHERE b.id = :bookId")
    List<Book> findAllByBookId(Long bookId);


}
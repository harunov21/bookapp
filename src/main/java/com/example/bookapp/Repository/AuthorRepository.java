package com.example.bookapp.Repository;

import com.example.bookapp.Model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorRepository extends JpaRepository<Author,Long> {
    @Query(value = "select * from author where email=?",nativeQuery = true)
    Optional<Author> findByEmail(String email);

}

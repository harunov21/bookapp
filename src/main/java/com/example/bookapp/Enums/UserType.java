package com.example.bookapp.Enums;

public enum UserType {
    AUTHOR,
    CUSTOMER
}

package com.example.bookapp.Controller;

import com.example.bookapp.Dto.BooksRequestDto;
import com.example.bookapp.Dto.CustomerLogInRequestDto;
import com.example.bookapp.Dto.CustomerRequestDto;
import com.example.bookapp.Dto.CustomerSignInRequestDto;
import com.example.bookapp.Model.Book;
import com.example.bookapp.Model.Customer;
import com.example.bookapp.Service.CustomerService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")

public class CustomerController {
    private final CustomerService customerService;

    @PostMapping
    public String signIn(@Valid @RequestBody CustomerSignInRequestDto customerSignInRequestDto){
        customerService.signIn(customerSignInRequestDto);
        return "Sign in successfully";
    }

    @PostMapping("/login")
    public ResponseEntity<String> logIn(@Valid @RequestBody CustomerLogInRequestDto customerLogInRequestDto){
       return customerService.login(customerLogInRequestDto);
    }

    @GetMapping
    public List<CustomerRequestDto> getAllBooksForCustomer(){
        return customerService.getAllBooksForCustomer();
    }

    @GetMapping("/{customerId}/book/{bookName}")
    public ResponseEntity<BooksRequestDto> getBookName(@PathVariable Long customerId
            ,@PathVariable String bookName) {
        BooksRequestDto book = customerService.getBookName(customerId, bookName);
        if (book != null) {
            return ResponseEntity.ok(book);

        } else {
           return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{customerId}/book/{bookId}")
    public Customer CustomerToBooks(

            @PathVariable Long customerId,
            @PathVariable Long bookId
    ){
        return customerService.customerToBooks(customerId, bookId);
    }

}

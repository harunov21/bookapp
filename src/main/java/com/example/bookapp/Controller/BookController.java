package com.example.bookapp.Controller;

import com.example.bookapp.Dto.BooksRequestDto;
import com.example.bookapp.Model.Book;
import com.example.bookapp.Service.BookService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;


    @PostMapping("/save")
    public Long saveBook(@Valid @RequestBody BooksRequestDto booksRequestDto){
       return bookService.saveBook(booksRequestDto);

    }

    @PostMapping("/{bookId}/customers/{customerId}")
    public ResponseEntity<String> addCustomerToBook(@PathVariable Long bookId,
                                                    @PathVariable Long customerId) {
        try {
            bookService.addCustomerToBook(bookId, customerId);
            return ResponseEntity.ok("Customer added to book successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occurred: " + e.getMessage());
        }
    }

    @PutMapping("/{bookId}")
    public ResponseEntity<BooksRequestDto> updateBook(@PathVariable Long bookId,
                                                      @RequestBody BooksRequestDto booksRequestDto) {
        return bookService.updateBooks(bookId, booksRequestDto);
    }

    @GetMapping(value = {"/getbook", "/{bookId}"})
    public List<BooksRequestDto> GetBooks(@PathVariable(required = false) Long bookId) {
        return bookService.getBookDetails(bookId);
    }
}
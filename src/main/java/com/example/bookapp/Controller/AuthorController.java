package com.example.bookapp.Controller;

import com.example.bookapp.Dto.AuthorLogInRequestDto;
import com.example.bookapp.Dto.AuthorSignInRequestDto;
import com.example.bookapp.Dto.BooksRequestDto;
import com.example.bookapp.Model.Book;
import com.example.bookapp.Service.AuthorService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("author")
@RequiredArgsConstructor
public class AuthorController {
    private final AuthorService authorService;

    @PostMapping("/signin")
    public String SignIn (@Valid @RequestBody AuthorSignInRequestDto authorSignInRequestDto){
        authorService.signIn(authorSignInRequestDto);
        return "Sign in successfully";
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@Valid @RequestBody AuthorLogInRequestDto authorLogInRequestDto){
        return authorService.login(authorLogInRequestDto);
    }

    @GetMapping("/books/{authorId}")
    public ResponseEntity<List<BooksRequestDto>> getAuthorBooks(@PathVariable Long authorId) {
        List<BooksRequestDto> authorBooks = authorService.getAuthorBooks(authorId);
        return ResponseEntity.ok(authorBooks);
    }

    @GetMapping("/{authorId}/books/{bookName}")
    public ResponseEntity<BooksRequestDto> getBookByName(@PathVariable Long authorId, @PathVariable String bookName) {
        BooksRequestDto book = authorService.getBookByName(authorId, bookName);
        if (book != null) {
            return ResponseEntity.ok(book);
        } else {
            return ResponseEntity.notFound().build();
        }

    }
    @PutMapping("/{authorId}/books/{bookId}")
    public ResponseEntity<Book> updateBook(@PathVariable Long authorId, @PathVariable Long bookId, @RequestBody BooksRequestDto bookRequestDto) {
        Book updatedBook = authorService.updateBook(authorId, bookId, bookRequestDto);
        return ResponseEntity.ok(updatedBook);
    }
}



package com.example.bookapp.Service;

import com.example.bookapp.Dto.BooksRequestDto;
import com.example.bookapp.Model.Book;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface BookService {
    Long saveBook(BooksRequestDto booksRequestDto);

    ResponseEntity<BooksRequestDto> updateBooks(Long bookId, BooksRequestDto updatedBookDto);


    List<BooksRequestDto> getBookDetails(Long bookId);

    void addCustomerToBook(Long bookId, Long customerId);
}


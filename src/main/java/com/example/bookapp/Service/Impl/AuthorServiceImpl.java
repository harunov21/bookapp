package com.example.bookapp.Service.Impl;

import com.example.bookapp.Dto.AuthorLogInRequestDto;
import com.example.bookapp.Dto.AuthorSignInRequestDto;
import com.example.bookapp.Dto.BooksRequestDto;
import com.example.bookapp.Enums.UserType;
import com.example.bookapp.Exception.NotFoundException;
import com.example.bookapp.Exception.ResourceNotFoundException;
import com.example.bookapp.Model.Author;
import com.example.bookapp.Model.Book;
import com.example.bookapp.Repository.AuthorRepository;
import com.example.bookapp.Repository.BookRepository;
import com.example.bookapp.Service.AuthorService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {
    private final AuthorRepository authorRepository;
    private final ModelMapper modelMapper;
    private final BookRepository bookRepository;
    @Override
    public void signIn(AuthorSignInRequestDto authorSignInRequestDto) {
        modelMapper.map(authorSignInRequestDto, Author.class);
        Author author = Author.builder()
                .name(authorSignInRequestDto.getName())
                .surname(authorSignInRequestDto.getSurname())
                .email(authorSignInRequestDto.getEmail())
                .password(authorSignInRequestDto.getPassword())
                .userType(UserType.AUTHOR)
                .build();
        authorRepository.save(author);
    }

    @Override
    public ResponseEntity<String> login(AuthorLogInRequestDto authorLogInRequestDto) {
        Optional<Author> authorOptional = authorRepository.findByEmail(authorLogInRequestDto.getEmail());

        if (authorOptional.isPresent()) {
            Author author = authorOptional.get();
            if (author.getPassword().equals(authorLogInRequestDto.getPassword())) {
                author.setIsActive(true);
                authorRepository.save(author);
                return ResponseEntity.ok("Login successful");
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid credentials");
            }
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Author not found");
        }
    }
    @Override
    public Author findById(Long id) {
        Optional<Author> byId = authorRepository.findById(id);
        byId.orElseThrow(() -> new ResourceNotFoundException("Resource not found"));
        return byId.get();
    }

    @Override
    public List<BooksRequestDto> getAuthorBooks(Long authorId) {
        Author author = authorRepository.findById(authorId)
                .orElseThrow(() -> new NotFoundException("Author not found: " + authorId));

        List<Book> books = author.getBooks();
        return books.stream()
                .map(book -> modelMapper.map(book, BooksRequestDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public BooksRequestDto getBookByName(Long authorId, String bookName) {
        Author author = authorRepository.findById(authorId)
                .orElseThrow(() -> new NotFoundException("Author not found: " + authorId));

        Book book = author.getBooks().stream()
                .filter(b -> b.getTitle().equals(bookName))
                .findFirst()
                .orElseThrow(() -> new NotFoundException("Book not found: " + bookName));

        return modelMapper.map(book, BooksRequestDto.class);
    }

    @Override
    public Book updateBook(Long authorId, Long bookId, BooksRequestDto bookRequestDto) {
        Author author = authorRepository.findById(authorId)
                .orElseThrow(() -> new NotFoundException("Author not found: " + authorId));

        Book book = author.getBooks().stream()
                .filter(b -> b.getId().equals(bookId))
                .findFirst()
                .orElseThrow(() -> new NotFoundException("Book not found: " + bookId));

        modelMapper.map(bookRequestDto, book);
        return bookRepository.save(book);
    }
}

















package com.example.bookapp.Service.Impl;

import com.example.bookapp.Dto.BooksRequestDto;
import com.example.bookapp.Exception.NotFoundException;
import com.example.bookapp.Model.Author;
import com.example.bookapp.Model.Book;
import com.example.bookapp.Model.Customer;
import com.example.bookapp.Repository.BookRepository;
import com.example.bookapp.Repository.CustomerRepository;
import com.example.bookapp.Service.AuthorService;
import com.example.bookapp.Service.BookService;
import com.example.bookapp.Service.CustomerService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final ModelMapper modelMapper;
    private final BookRepository bookRepository;
    private final AuthorService  authorService;
    private final  CustomerService customerService;
    private final CustomerRepository customerRepository;

    public Long saveBook(BooksRequestDto booksRequestDto) {
        Author author = authorService.findById(booksRequestDto.getAuthorId());
        Customer customer = customerService.findById(booksRequestDto.getCustomerId());
        Book book = Book.builder()
                .title(booksRequestDto.getTitle())
                .customer(customer)
                .author(author)
                .build();
        bookRepository.save(book);
        return book.getId();
    }

    public ResponseEntity<BooksRequestDto> updateBooks(Long bookId, BooksRequestDto updatedBookDto) {
        Optional<Book> bookOptional = bookRepository.findById(bookId);

        if (!bookOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Book existingBook = bookOptional.get();
        modelMapper.map(updatedBookDto, existingBook);
        Book savedBook = bookRepository.save(existingBook);
        BooksRequestDto savedBookDto = modelMapper.map(savedBook, BooksRequestDto.class);
        return ResponseEntity.ok(savedBookDto);
    }

    @Override
    public List<BooksRequestDto> getBookDetails(Long bookId) {
        return null;
    }


    @Override
    public void addCustomerToBook(Long bookId, Long customerId) {
        Book book = bookRepository.findById(bookId)
                .orElseThrow(() -> new NotFoundException("Book not found with ID: " + bookId));
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new NotFoundException("Customer not found with ID: " + customerId));
        book.getCustomers().add(customer);
        bookRepository.save(book);
    }
}








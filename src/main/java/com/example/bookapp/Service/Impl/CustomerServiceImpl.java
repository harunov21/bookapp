package com.example.bookapp.Service.Impl;

import com.example.bookapp.Dto.BooksRequestDto;
import com.example.bookapp.Dto.CustomerLogInRequestDto;
import com.example.bookapp.Dto.CustomerRequestDto;
import com.example.bookapp.Dto.CustomerSignInRequestDto;
import com.example.bookapp.Enums.UserType;
import com.example.bookapp.Exception.NotFoundException;
import com.example.bookapp.Exception.ResourceNotFoundException;
import com.example.bookapp.Model.Book;
import com.example.bookapp.Model.Customer;
import com.example.bookapp.Repository.BookRepository;
import com.example.bookapp.Repository.CustomerRepository;
import com.example.bookapp.Service.CustomerService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;
    private final ModelMapper modelMapper;
    private final BookRepository bookRepository;
    @Override
    public List<CustomerRequestDto> getAllBooksForCustomer() {
        List<Customer> customers = customerRepository.findAll();
        List<CustomerRequestDto> cdList = new ArrayList<>();
        customers.forEach(customer -> {
            CustomerRequestDto customerRequestDto =
                    modelMapper.map(customer,CustomerRequestDto.class);
            cdList.add(customerRequestDto);
        });
        return cdList;
    }
    @Override
    public void signIn(CustomerSignInRequestDto customerSignInRequestDto) {
        modelMapper.map(customerSignInRequestDto, Customer.class);
        Customer customer=Customer.builder()
                .name(customerSignInRequestDto.getName())
                .surname(customerSignInRequestDto.getSurname())
                .email(customerSignInRequestDto.getEmail())
                .password(customerSignInRequestDto.getPassword())
                .userType(UserType.CUSTOMER)
                .build();
        customerRepository.save(customer);
    }

    public ResponseEntity<String> login(CustomerLogInRequestDto customerLogInRequestDto) {
        Optional<Customer> customerOptional = customerRepository.findByEmail(customerLogInRequestDto.getEmail());

        if (customerOptional.isPresent()) {
            Customer customer = customerOptional.get();
            if (customer.getPassword().equals(customerLogInRequestDto.getPassword())) {
                customer.setActive(true);
                customerRepository.save(customer);
                return ResponseEntity.ok("Login successful");
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid credentials");
            }
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
        }
    }

    @Override
    public Customer findById(Long id) {
        Optional<Customer> byId = customerRepository.findById(id);
        byId.orElseThrow(() -> new ResourceNotFoundException("Resource Not Found"));
        return byId.get();
    }

    @Override
    public BooksRequestDto getBookName(Long customerId , String bookName) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(()-> new EntityNotFoundException("Customer not found" + customerId));

        Book book = customer.getBooks().stream().
                filter(b -> b.getTitle().equals(bookName))
                .findFirst().
                orElseThrow(() -> new EntityNotFoundException("Book not found" + bookName));
        return modelMapper.map(book,BooksRequestDto.class);
    }

    @Override
    public Customer customerToBooks(Long customerId, Long bookId) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new NotFoundException("Customer not found: " + customerId));
        Book book = bookRepository.findById(bookId)
                .orElseThrow(() -> new NotFoundException("Book not found: " + bookId));
        customer.getBooks().add(book);
        customerRepository.save(customer);

        return customer;
    }


}

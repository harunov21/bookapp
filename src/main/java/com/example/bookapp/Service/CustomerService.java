package com.example.bookapp.Service;

import com.example.bookapp.Dto.BooksRequestDto;
import com.example.bookapp.Dto.CustomerLogInRequestDto;
import com.example.bookapp.Dto.CustomerRequestDto;
import com.example.bookapp.Dto.CustomerSignInRequestDto;
import com.example.bookapp.Model.Book;
import com.example.bookapp.Model.Customer;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    List<CustomerRequestDto> getAllBooksForCustomer();

    void signIn(CustomerSignInRequestDto customerSignInRequestDto);

    ResponseEntity<String> login(CustomerLogInRequestDto customerLogInRequestDto);

    Customer customerToBooks(Long customerId, Long bookId);

    Customer findById(Long customerId);

    BooksRequestDto getBookName(Long customerId,String bookName);
}

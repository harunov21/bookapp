package com.example.bookapp.Service;

import com.example.bookapp.Dto.AuthorLogInRequestDto;
import com.example.bookapp.Dto.AuthorSignInRequestDto;
import com.example.bookapp.Dto.BooksRequestDto;
import com.example.bookapp.Model.Author;
import com.example.bookapp.Model.Book;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AuthorService {
    void signIn(AuthorSignInRequestDto authorSignInRequestDto);

    ResponseEntity<String> login(AuthorLogInRequestDto authorLogInRequestDto);

    Author findById(Long id);
    

    List<BooksRequestDto> getAuthorBooks(Long authorId);


    BooksRequestDto getBookByName(Long authorId, String bookName);

    Book updateBook(Long authorId, Long bookId, BooksRequestDto bookRequestDto);
}

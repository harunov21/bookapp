FROM openjdk:17
COPY build/libs/Bookapp-0.0.1-SNAPSHOT.jar /book-app/
CMD ["java", "-jar", "/book-app/Bookapp-0.0.1-SNAPSHOT.jar"]
